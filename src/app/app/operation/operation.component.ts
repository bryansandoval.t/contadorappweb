import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { setOperation, setValues } from 'src/app/config/global.action';


@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {
  formOperation: any;
  errorMessage: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<any>
  ) { }

  type$: Observable<any> = this.store.select(state => state.type);
  type: any;
  result$: Observable<any> = this.store.select(state => state.result);
  result: any
  dataClean$: Observable<any> = this.store.select(state => state.dataClean);


  ngOnInit(): void {
    this.formOperation = this.formBuilder.group({
      numberA: new FormControl('', Validators.required),
      numberB: new FormControl('', Validators.required),
    })

    this.type$.subscribe((type) => {
      this.type = type;
    })
    this.result$.subscribe((result) => {
      this.result = result;
    })
    this.dataClean$.subscribe(() => {
      this.formOperation.setValue({
        numberA: '',
        numberB: '',
      });
      this.result = '';
    })
  }

  onSubmit() {
    if(this.formOperation.status == 'VALID'){
      this.store.dispatch(new setValues({ valuesOperation: this.formOperation.value }))
      this.store.dispatch(new setOperation())
      this.errorMessage = false;
    }else{
      this.errorMessage = true;
    }
  }

}
