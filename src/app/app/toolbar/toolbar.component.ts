import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { setDataClean, setOperationResult } from 'src/app/config/global.action';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor(
    private store: Store<any>,
  ) { }

  ngOnInit(): void {
  }

  dataClean(){
    this.store.dispatch(new setDataClean({dataClean: {}, valuesOperation: {}, result: 0 }))
  }
}
