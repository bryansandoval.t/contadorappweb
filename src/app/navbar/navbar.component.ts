import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { typeOperation } from '../config/global.action';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {  
  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<any>,
  ) { }
  isHandset = false

  ngOnInit() {
  }

  operation(type: any) {
    this.store.dispatch(new typeOperation({ type }))
  }
}
