import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

export enum ActionTypes {
    setValues = '[global] setValues',
    setCloseNavbar = '[global] setCloseNavbar',
    setOperationResult = '[global] setOperationResult',
    typeOperation = '[global] typeOperation',
    setOperation = '[global] setOperation',
    setDataClean = '[global] setDataClean',
}
//-----------------------
//Redux operations
export class setValues implements Action {
    readonly type = ActionTypes.setValues;
    constructor(public payload: { valuesOperation: {} }) { }
}
export class setDataClean implements Action {
    readonly type = ActionTypes.setDataClean;
    constructor(public payload: { dataClean: {}, valuesOperation: {}, result: number }) { }
}
export class setCloseNavbar implements Action {
    readonly type = ActionTypes.setCloseNavbar;
    constructor(public payload: { closeNavbar: boolean }) { }
}
export class setOperationResult implements Action {
    readonly type = ActionTypes.setOperationResult;
    constructor(public payload: { result: number }) { }
}
export class typeOperation implements Action {
    readonly type = ActionTypes.typeOperation;
    constructor(public payload: { type: string }) { }
}

//------------------------
//Operations effects
export class setOperation implements Action {
    readonly type = ActionTypes.setOperation;
}

export type actions =
    setValues
    | setCloseNavbar
    | setOperationResult
    | typeOperation
    | setOperation