import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { ActionTypes, actions } from './global.action';
import { environment } from '../../environments/environment';



export interface State {
}

export const reducers: ActionReducerMap<State> = {
};

export interface GlobalState {
  result: number;
  closeNavbar: boolean;
  valuesOperation: Object;
  type: string;
  dataClean: Object;
}

export const inicialStateGlobal: GlobalState = {
  result: 0,
  closeNavbar: true,
  valuesOperation: {},
  type: "",
  dataClean: {}
};

export function globalReducer(state: GlobalState = inicialStateGlobal, action: actions): GlobalState {
  return state;
}

export function globalMetaReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state: State, action: any) {
    switch (action.type) {
      case ActionTypes.setValues:
        return {
          ...state,
          valuesOperation: action.payload.valuesOperation
        };
      case ActionTypes.typeOperation:
        return {
          ...state,
          type: action.payload.type
        };
      case ActionTypes.setDataClean:
        return {
          ...state,
          dataClean: action.payload.dataClean,
          valuesOperation: action.payload.valuesOperation,
          result: action.payload.result
        };
      case ActionTypes.setOperationResult:
        return {
          ...state,
          result: action.payload.result
        };
      case ActionTypes.setOperationResult:
        return {
          ...state,
          closeNavbar: action.payload.closeNavbar
        };
      default:
        return reducer(state, action);
    }

  };
}


export const metaReducers: MetaReducer<State>[] = !environment.production ? [globalMetaReducer] : [globalMetaReducer];

