import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, EffectNotification, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap, withLatestFrom, map, mergeMap, filter, exhaustMap, catchError, concatMap, switchMap, delay, take, takeUntil } from 'rxjs/operators';
import { ActionTypes, setOperationResult } from './global.action';
import { State } from './global.reducer';

@Injectable()

export class GeneralEffects {
  constructor(
    private actions$: Actions,
    //private router: Router,
    private store$: Store,
  ) { }

  setOperation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.setOperation),
      withLatestFrom(this.store$.select((state: any) => state.valuesOperation), this.store$.select((state: any) => state.type)),
      map((action) => {
        let result: number = 0;
        switch (action[2]) {
          case 'sum': {
            result = action[1].numberA + action[1].numberB
            break;
          }
          case 'subtraction': {
            result = action[1].numberA - action[1].numberB
            break;
          }
          case 'multiplication': {
            result = action[1].numberA * action[1].numberB
            break;
          }
          case 'potency': {
            result = Math.pow(action[1].numberA, action[1].numberB)
            break;
          }
        }
        return new setOperationResult({ result: result })
      })
    ), {});
}

export const effects = [
  GeneralEffects,
];